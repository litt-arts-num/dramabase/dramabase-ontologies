# dramabase-ontologies

Voici les trois ontologies utilisées par le projet Dramabase :

* L'ontologie Erlangen `CRM /OWL` (implémentation en OWL de l'ontologie du CIDOC CRM). Documentation accessible sur http://erlangen-crm.org/docs/ecrm/current/index.html


* L'ontologie Erlangen `FRBRoo` (implémentation en OWL de l'extension FRBRoo du CIDOC CRM )   Documentation accessible sur http://erlangen-crm.org/docs/efrbroo/latest/


* L'ontologie du projet Dramabase `DRAM`, conçue comme une extension de FRBRoo, utilisant également quelques propriétés de `FOAF` "modularisé". La documentation est inclue dans les commentaires du fichier.
