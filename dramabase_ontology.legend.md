# Nœuds

| couleur | signification |
| -- | -- |
| rouge | instance |
| blanc | valeur littérale |
| bleu | classe |
| bleu clair | classe parente et rdfs:subClassOf |
| vert | propriété |
| vert clair | propriété parente et rdfs:subPropertyOf |

# Liens

| couleur | localisation |
| -- | -- |
| rouge | cesar-rdf/* |
| noir plein | dramabase-ontologies/dramabase-ontology.ttl |
| noir pointillé | dramabase-ontologies/efrboo-121016.ttl |

# Principe

Sont indiqués
* pour chaque instance :
  * rdfs:label
  * a
  * Data Properties
  * Object Properties
* pour chaque classe :
  * rdfs:subClassOf si la classe n'est pas ecrm:
* pour chaque propriété :
  * rdfs:subPropertyOf si la propriété n'est pas  ecrm:, rdfs: ou foaf:

# Préfixes

| préfixe | IRI |
| -- | -- |
| ecrm | http://erlangen-crm.org/current/ |
| rdfs | http://www.w3.org/2000/01/rdf-schema# |
| crmpc | http://dramabase/ontology/crm_property_classes/ |
| efrbroo | http://erlangen-crm.org/efrbroo/ |
| foaf | http://xmlns.com/foaf/0.1/ |
| dram | http://dramabase/ontology/ |
| cesar | http://cesar/data/ |
| xsd | http://www.w3.org/2001/XMLSchema# |
| crmpc | http://dramabase/ontology/crm_property_classes/ |
